# Synopsis

logcat2sys is a Logcat to Syslog (RFC5242) converter.

It accepts a stream of logcat-formatted messages, and sends them over
TCP or UDP using the RFC5242 format.

# Usage

The host and port and mandatory.

Additionally, you can specify the delay between each packet, and the protocol.

In order to make reading/aggregating logs easier, you can specify a `log-id`, 
which is then added to the log line, so you can grep it, aggregate it, configure
Promtail to parse it, etc.

## Example

```sh
adb logcat | logcat2sys --proto TCP --log-id second-run my-promtail 1514
```

## Options

```
usage: logcat2sys.py [-h] [--proto PROTO] [--delay DELAY] [--log-id LOG_ID] [--debug] hostname port

logcat to syslog conversion.

positional arguments:
  hostname         hostname to connect to
  port             hostname's port

optional arguments:
  -h, --help       show this help message and exit
  --proto PROTO    protocol (TCP/UDP, default UDP)
  --delay DELAY    sleep between logs
  --log-id LOG_ID  logcat2sys log id
  --debug          enable debugging
```
